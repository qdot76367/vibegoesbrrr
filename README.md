# Vibe Goes Brrr~ v0.4.4

- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Setup](#setup)
  * [Dynamic Penetration System](#dynamic-penetration-system)
  * [Touch Zones](#touch-zones)
- [Usage](#usage)
- [Help and Feedback](#help-and-feedback)
- [Settings](#settings)
- [Advanced Usage](#advanced-usage)
  * [Expression Parameters](#expression-parameters)
  * [Thrust Vectors](#thrust-vectors)
- [Troubleshooting](#troubleshooting)
- [Changelog](#changelog)
- [Third Party Licenses](#third-party-licenses)

## Prerequisites

There are a few prerequisites needed for this mod to work:

- **A compatible toy**

- **A Bluetooth connection**, either built-in or a dongle supporting Bluetooth Low Energy

- **Intiface Desktop**, which manages the connection between your PC and your toys

- **MelonLoader**, a mod loader for VRChat

- **UIExpansionKit**, a VRChat mod that will let you configure settings in-game (Optional but recommended)

#### A Compatible Toy

Any Bluetooth connected toys supported by Buttplug.io are compatible with the mod, though Lovense toys are recommended as they seem to be the most reliable by far! 

Check out their full range of remote controlled toys here: https://www.lovense.com/r/wyn5t0

You can find a full list of compatible toys here: https://iostindex.com/?filtersChanged=1&filter0ButtplugSupport=4

#### A Bluetooth connection

You will need some kind of Bluetooth connectivity to connect to your toys. Many computers come with this built-in, but if you're having connectivity issues it might be worth investing in a decent, dedicated Bluetooth dongle. Your Bluetooth connection needs to support Bluetooth Low Energy.

The Buttplug.io developers recommend the **Plugable USB Bluetooth 4.0** adapter, as it is known to work and easy to come by on Amazon. 

Known **broken** Bluetooth dongles include **Insignia** branded ones, and a lot of **Bluetooth 5.0** adapters.

#### Intiface Desktop

Download and install [Intiface Desktop](https://intiface.com/desktop/). 

> Intiface Desktop is an open-source, cross-platform application that acts as a hub for sex hardware access. The mod uses this as a relay to connect to your toys.

**You do not need to keep Intiface running to use Vibe Goes Brrr, but it's useful to open it now and then and check for updates for connection stability fixes and compatibility with new toys!**

#### MelonLoader

Download the installer and follow the installation instructions at the [MelonLoader Wiki](https://melonwiki.xyz/#/?id=requirements).

**Please be aware that modding your VRChat client is not endorsed by the VRChat team and against their ToS. Use this at your own risk.**

For general support when it comes to VRChat Modding, join the [VRChat Modding Group discord](https://discord.gg/7EQCmgrUnH).

#### UIExpansionKit

Download `UIExpansionKit.dll` and drop it in your VRChat "Mods" folder.

https://github.com/knah/VRCMods/releases (keep scrolling until you find UIExpansionKit.dll in one of the "Assets" folds)

UIExpansionKit is a mod that will add an additional settings menu to your regular VRChat settings menu, allowing you to change mod settings.

This mod is **optional, but highly recommended** so that you may tweak the many settings and quick-toggles provided by Vibe Goes Brrr~


## Installation

Install the mod by dropping `VibeGoesBrrr.dll` into your VRChat "Mods" folder, e.g. `C:\Program Files (x86)\Steam\steamapps\common\VRChat\Mods`.


## Setup

### Dynamic Penetration System

Vibe Goes Brrr~ now comes with built-in support for [Dynamic Penetration System by Raliv](https://gumroad.com/l/lwthuB)! We strongly recommend this method of setup, but [Touch Zones](#touch-zones) are still available as a more general alternative for *any* type of touch, and both will work together for great effect!

After you've set up either a Penetrator or Orifice by following their guide, it should work out of the box with **no additional setup needed**!

Your toys will automatically bind to a matching penetrator or orifice, based on if your toy is classed as a "giver" or a "taker". This is decided based on the [IoSTIndex.com](https://iostindex.com/?filtersChanged=1&filter0ButtplugSupport=4) database of compatible toys. If you want to override this behaviour, simply rename your penetrator or orifice object in the format of "ThrustVector Hush", where "Hush" would be the name of the toy you wish it to bind to. See the [Thrust Vectors](#thrust-vectors) section for more details.

### Touch Zones

**Touch Zones** are a type of sensor that triggers from *any* touch by other VRC chatacters. These are more general and gives you more freedom in ways that your vibrations can be triggered. In order to set these up, you will need to add the included prefab to your VRC character.

Start by importing the `VibeGoesBrrr.unitypackage` by double-clicking or drag-and-dropping it into your Unity project. Multiple prefabs will be imported into your `Assets/VibeGoesBrrr` folder:

#### TouchZone

This is the "one size fits all" version of the prefab. You may add one or multiple of these anywhere in your character's armature (though the `Hips` bone is recommended in most cases). Simply drag-and-drop it into a bone in your armature, and adjust it as necessary.

In order to bind a certain TouchZone to a specific toy, rename the object in the format of "TouchZone [Toy Name]", e.g. "TouchZone Hush" or "TouchZone Max". A partial name is enough. If you're unsure what your toy is named, a list of connected toys will be printed in the Melon Loader console on start. Alternatively, if you only indend on using one toy at a time, leaving the name as "TouchZone" will trigger *all* connected toys in unison.

If you have a device with multiple motors, like the Lovense Edge, you may control each motor individually by adding an the hash symbol (#) and a number at the end of the sensor name. For example, you may set up two sensors named "TouchZone Edge #1" and "TouchZone Edge #2" to control the two motors of the Lovense Edge separately!

The vibration intensity is controlled distance of touch. This means that the deeper something enters the Touch Zone, the more intense the vibrations will be. To make this work properly, make sure that the "dots" visible around the edge of the Touch Zone points **outwards**.

![](docs/2020-12-11-02-10-38-image.png)

You may tweak the `Size` and `Far` parameters of the **Camera** component in order to resize the Touch Zone. It's recommended to increase the `Far` parameter more than you think you need and move the sensor deeper into the body, since the camera bounds are exactly as the box bounds are visualized in Unity. Changing any other parameters is not recommended.

![](docs/2020-12-11-02-13-58-image.png)

If you feel like your Touch Zones aren't where they're supposed to be once in VRChat, you can turn on the `Setup Mode` option in your mod settings, to make the Touch Zones visible in-game, which might help you with positioning.

##### TouchZone Hush, TouchZone Max

These prefabs are provided as an example for the **Lovense Hush** and the **Lovense Max.** They're set up to fit to the `Hips` bone in your armature, though depending on how your character is fashioned your mileage may vary.


## Usage

If you've reached this far, you should be good to go! When you start VRChat, the mod should automatically connect to your toys and the Touch Zones should trigger them. If you set up [Dynamic Penetration System](#dynamic-penetration-system), vibrations should trigger once the tip of a penetrator touches the entrance of any orifice, even in-world ones!

Important to know is that **Touch Zones will only activate by touch from other people and grabbable objects.** If you want to test your Touch Zones you should join a world that has either of those :)


## Help and Feedback

Feel free to join and post in the [Buttplug.io #vrchat-neos-etc channel on Discord](https://discord.gg/eCtyTMPS2U) if you are having any kind of trouble, have bugs to report, or have any other kind of feedback!

Make sure to name drop Vibe Goes Brrr~, since this is a general channel used for a lot of other Buttplug.io integrations as well.


## Settings

If you installed UIExpansionKit earlier, some extra options should show up in the bottom-right of your VRC settings menu. Clicking "Pin" next to a setting will add an easily accessible toggle to the main menu. Pin both "Touch Vibrations" and "Idle Vibrations" for easy access :)

- **Touch Vibrations**
  Toggles on/off vibrations from [Touch Zones](#touch-zones).

- **Thrust Vibrations**
  Toggles on/off vibrations from [Dynamic Penetration System](#dynamic-penetration-system) or [Thrust Vectors](#thrust-vectors).

- **Idle Vibrations**
  Toggles on/off idle vibrations at the level set by "Idle Vibration Intensity %"

- **Touch Feedback**
  Enable haptic feedback in your hand controllers with varying vibrational intensity when touching other player's Touch Zones. This only works on people on your friends list.

- **Max Vibration Intensity %**
  Limits vibrations to a maximum percentage value.

- **Update Frequency**
  How many times per second sensors will be checked for updates. Lower this if you feel it's dragging down your framerate, or increase it if you feel it doesn't update often enough!

- **Setup Mode** 
  Visualizes your Touch Zones in-game. Helpful for testing, if you feel like your Touch Zones aren't where you expect them to be.

- **Expression Parameters**
  This lets you control wether vibration intensity will be written to expression parameters on your Avatar. See [Expression Parameters](#expression-parameters) for more info.

- **Restart Intiface**
  Checking this will restart the internal Intiface server. Use this if your device or Intiface encounters an error and refuses to reconnect.

![](docs/UIExpansionKit.png)


## Advanced Usage

### Expression Parameters

If you enable the [Expression Parameters](#settings) setting, the mod will write the current vibration intensity of your Touch Zones into equally named expression parameters. This will let you dynamically animate your character and control vibrations manually through your avatar expression menu!

Sensor values will be written into a matching expression parameter if it exists on your character. For example, add a `TouchZone Hush` float parameter and it will be continuously updated with the current intensity of the "TouchZone Hush" sensor. This works the same way for Thrust Vectors.

The special expression parameter named `Intensity` will also be filled with the maximum intensity of all your zones, if you're not interested in specific ones.

### Thrust Vectors

A **Thrust Vector** is an alternative type of sensor that bridges compatibility with Dynamic Penetration System. Thrust Vector objects will look for other Thrust Vectors in the world, and if the **distance between them** is **shorter** than the **length of the mesh of the "giving" Thrust Vector**, their assigned devices will vibrate accordingly.

If an avatar has [Dynamic Penetration System](#dynamic-penetration-system) installed, it will have Thrust Vectors set up for it automatically.

Thrust Vectors will auto-bind to toys based on if the toy is classed as a "giver" or a "taker". In general, wearable toys are classed as givers, while insertable toys are classed as takers. Only Thrust Vectors that are direct children of the bone types `Spine`, `Hips`, `Chest` and `UpperChest` are considered for auto-binding. If you want to disable automatic binding, name the object "ThrustVector Inactive".

To set up a Thrust Vector manually, create an object with the name according to the format of "ThrustVector Hush" (where "Hush" is the toy you want it to bind to). Alternatively, you may leave out the toy name to have it be auto-assigned a toy based on if it's a "giver" or a "taker". Adding a mesh to this object will class it as a "giver". If the object has no mesh, it's a "taker". The mesh length is calculated as the distance between the mesh origin and its maximum point on the **+Z axis**.


## Troubleshooting

### First steps

- Check the Melon Loader console for log messages starting with `[VibeGoesBrrr]`

- Double-check that your Touch Zones are set-up correctly and are where you expect them to be. For some avatars the "Hips" bone might not be the best place to attach them. Enabling [Setup Mode](#settings) in mod settings will allow you to see where the Touch Zones move with your avatar once you're in-game.

- Make sure that your device shows up when manually scanning for Bluetooth devices in your PC's Bluetooth settings, but **do not pair it**. If it doesn't show up here, something is wrong with your Bluetooth connection.

- If you installed UIExpansionKit, make sure the [Touch Vibrations](#settings) setting is enabled in Mod Settings.

- Try toggling the [Idle Vibrations](#settings) setting to see if your toy is connected and working.

- If the toy does not respond it might have encountered an error. Try toggling the [Restart Intiface](#settings) setting, and it might reconnect.

- You may test your toy outside of VRChat by opening up Intiface Desktop, clicking "Start Server", and visiting https://playground.buttplug.world

### Disconnections

Sometimes toys may lose connection somewhere along the line from the hardware itself, through the Bluetooth stack and or the Buttplug.io connectivety software. This can happen for various reasons, but in most cases the device will recover within a few seconds.

If your device doesn't automatically reconnect within 10 seconds, you can try restarting it manually.

Most connection problems are caused by the Bluetooth hardware itself. Consider investing a little bit more into a brand name Bluetooth dongle instead of going with the cheapest option.

Another culprit could be your PC's USB controller being overloaded. If you feel like your USB devices are only working intermittently or in weird combinations depending on which device you plug in first, you might need to invest in a USB hub with a built-in dedicated controller to take the load off your PC.

### VRChat crashing

This could happen for various reasons, but if you think it's related to Vibe Goes Brrr, please post a copy of your latest MelonLoader log file (resides in your `VRChat/MelonLoader/Logs` folder) in the [Buttplug.io #vrchat-neos-etc channel on Discord](https://discord.gg/eCtyTMPS2U)

### VRChat lagging after being online for a long time

This is most likely caused by a memory leak present in version 19 of the Intiface Engine and below. Make sure you're on version 20 or above! You can check this by starting Initface Desktop and looking at "Intiface Engine Version" in the settings.

![](docs/intiface-desktop_NJAsp600QI.png)


## Changelog

### v0.4.4
- DPS penetrators and orifices will now only auto-bind to toys when child of Spine, Hips, Chest and UpperChest bones in armature
- DPS auto-binding now takes parent constraints into account
- Fixed expression parameters not being visible to other players after latest VRChat update

### v0.4.3
- Now compatible with in-world DPS penetrators and orifices!
- Fixed orifices sometimes not being activated by other player's penetrators
- Fixed avatars using Mochie's Unity Shaders being invisible to other player's touch zones. If you are using them, a [temporary workaround is available](https://gitlab.com/jacefax/vibegoesbrrr/-/issues/39#note_555351445) to make you visible to players who are yet to update. 
- Fixed expression parameters getting stuck at current value when turning off the setting

### v0.4.2
- Tweaked intensity curve to make full vibration strength easier to reach
- Toys will no longer auto-bind to penetrators and orifices placed in hands, mouths, etc
- Added touch feedback to Dynamic Penetration System sensors
- Performance improvements to Dynamic Penetration System sensors
- Fixed global expression parameters being affected by other players sensors
- Reverted client-side prediction of expression parameters to reduce number of active sensors and improve performance

### v0.4.1
- Fixed "Human" penetrator prefabs not being detected
- Fixed penetrators and orifices with non-standard light ranges not being detected
- Added instructions on how to disable the automatic toy matching behaviour for a penetrator/orifice, see the [Dynamic Penetration System](#dynamic-penetration-system) of the instructions (tl;dr: simply rename it "ThrustVector Inactive")
- Fixed DPS penetrator length not being updated when mesh scale changes
- Future compatibility with next DPS version

### v0.4.0
- Added support for [Dynamic Penetration System by Raliv](https://gumroad.com/l/lwthuB). It should work pretty much out of the box! Check out the Dynamic Penetration System section of the instructions for more details!
- Touch Zones no longer need a device bound in order to update expression parameters, so now you can use them for general character animation purposes too! Maybe a shirt lifting animation? 👀
- Fixed more cases of devices getting stuck vibrating when they shouldn't
- Fixed more cases of Setup Mode boxes not disappearing 

### v0.3.4
- Fixed devices sometimes getting stuck vibrating when switching to avatars without any matching Touch Zones
- Fixed Setup Mode boxes not disappearing from disabled Touch Zones when Setup Mode is toggled off 
- Fixed crash at startup caused by the RestartIntiface setting being saved as true

### v0.3.3
- Fixed error spam when a device is connected while wearing an avatar without any matching Touch Zones

### v0.3.2
- Fixed Touch Zones not working after being disabled at load and later enabled through a toggle!
- Disable inactive Touch Zone cameras, to improve performance when Touch Zones aren't in use.
- Switched to stateful Touch Zone scanning, which should improve performance relative to v0.2.
- Added "Restart Intiface" option in settings. Toggle this setting if your device encounters an error and refuses to reconnect.
- Disabled buggy expression parameter _read_ functionality for the time being, since it was causing weird intensity oscillations. Expression parameters are still _written_ to, for use in animations. Parameter read should be back in v0.4.0.
- Renamed "Write Expression Parameters" setting to just "Expression Parameters".

### v0.3.1
- Fixed overeager device scanning that was overwhelming Intiface. Should stabilize device connections a lot!

### v0.3.0
- Added haptic feedback in hand controllers with varying vibration intensity when touching Touch Zones belonging to your friends!
- Toys with multiple vibrator motors can now be controlled by different Touch Zones, by appending a hash symbol and a number to the end of the Touch Zone name. For example, set up two Touch Zones with the names "TouchZone Edge #1" and "TouchZone Edge #2" to separate the motors of the Lovense Edge.
- Added "Setup Mode" option that visualizes Touch Zones in-game, to help when setting up Touch Zones for your avatar.
- Added vibration intensity write into expression parameters, allowing you to animate your character based on vibration intensity! Check out the "Advanced Usage" section of the guide for more information.
- Added vibration intensity read from expression parameters, giving you manual control over vibrations through the expression menu. 
- Quick 🎺 doot-doot 🎺 vibration when a toy is successfully bound to a Touch Zone.
- Added update check on startup, for both VibeGoesBrrr~ and Intiface Engine.
- Updated to the new Buttplug FFI library which should improve connection stability overall. If you encounter any errors, please report them in the [Buttplug.io Discord](https://discord.gg/eCtyTMPS2U) ❤

#### v0.2.1
- Many tweaks to hopefully improve connection stability!
- Device scanning is now on a 60 second timer, toggling the "Touch Vibrations" setting will start a manual scan.
- Toggling the "Touch Vibrations" setting will automatically restart the server if no devices are connected. If your device disconnects, try this and wait a few seconds!

#### v0.2.0
- Initial release


## Third Party Licenses

### [buttplug-rs-ffi](https://github.com/buttplugio/buttplug-rs-ffi)

```
Copyright (c) 2016-2020, Nonpolynomial Labs, LLC
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of buttplug nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```

### [IoST Index](https://github.com/blackspherefollower/IoSTIndex)

```
BSD 3-Clause License

Copyright (c) 2019, blackspherefollower
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
```
