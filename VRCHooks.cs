using System;
using System.Reflection;
using Harmony;

namespace VibeGoesBrrr
{
  class VRCHooks
  {
    public static event EventHandler<VRCPlayer> AvatarIsReady;
    public static event EventHandler<string> AssignPuppetChannel;
    public static event EventHandler<string> ClearPuppetChannel;
    public static string CurrentPuppeteerParameter = null;

    public static void OnApplicationStart(HarmonyInstance harmony)
    {
      // Hook OnVRCPlayerAwake
      harmony.Patch(typeof(VRCPlayer).GetMethod(nameof(VRCPlayer.Awake), BindingFlags.Public | BindingFlags.Instance), 
        postfix: new HarmonyMethod(typeof(VRCHooks).GetMethod(nameof(OnVRCPlayerAwake), BindingFlags.NonPublic | BindingFlags.Static)));

      // Hook action menu puppeteering
      // TODO: Keep this silent until we actually need it 
      // TODO: Xref scan?
      harmony.Patch(typeof(ActionMenu).GetMethod(nameof(ActionMenu.Method_Public_Void_Parameter_1), BindingFlags.Public | BindingFlags.Instance),
        postfix: new HarmonyMethod(typeof(VRCHooks).GetMethod(nameof(OnAssignPuppetChannel), BindingFlags.NonPublic | BindingFlags.Static)));
      harmony.Patch(typeof(ActionMenu).GetMethod(nameof(ActionMenu.Method_Public_Void_Parameter_0), BindingFlags.Public | BindingFlags.Instance), 
        postfix: new HarmonyMethod(typeof(VRCHooks).GetMethod(nameof(OnClearPuppetChannel), BindingFlags.NonPublic | BindingFlags.Static)));
    }

    private static void OnVRCPlayerAwake(VRCPlayer __instance)
    {
      // OnAvatarIsReady
      __instance.Method_Public_add_Void_MulticastDelegateNPublicSealedVoUnique_0(new Action(() => {
        if (__instance.prop_Player_0?.prop_ApiAvatar_0 != null) {
          AvatarIsReady?.Invoke(null, __instance);
        }
      }));
    }

    [HarmonyArgument(0, "param")]
    private static void OnAssignPuppetChannel(ActionMenu __instance, VRC.SDK3.Avatars.ScriptableObjects.VRCExpressionsMenu.Control.Parameter param)
    {
      Util.DebugLog($"Puppeteering start {param.name} {param.hash}");
      CurrentPuppeteerParameter = param.name;
      AssignPuppetChannel?.Invoke(null, param.name);
    }

    [HarmonyArgument(0, "param")]
    private static void OnClearPuppetChannel(ActionMenu __instance, VRC.SDK3.Avatars.ScriptableObjects.VRCExpressionsMenu.Control.Parameter param)
    {
      Util.DebugLog($"Puppeteering end {param.name} ({param.hash})");
      CurrentPuppeteerParameter = null;
      ClearPuppetChannel?.Invoke(null, param.name);
    }
  }
}