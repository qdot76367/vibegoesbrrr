using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnhollowerRuntimeLib;
using UnityEngine;
using static MelonLoader.MelonLogger;

namespace VibeGoesBrrr
{
  class ThrustVectorProvider : ISensorProvider, IDisposable
  {
    public class Giver : Sensor
    {
      public static Regex Pattern = new Regex(@"^\s*?(Thrust\s*Vector)\s*(.*)$", RegexOptions.IgnoreCase);

      public float mValue;
      public float mBaseLength;
      public MeshRenderer mMesh;

      public float Length {
        get {
          return mBaseLength * Vector3.Magnitude(mMesh.transform.TransformVector(mMesh.transform.forward));
        }
      }

      protected GameObject mGameObject;

      public Giver(string name, SensorOwnerType ownerType, GameObject gameObject, MeshRenderer mesh)
        : this(Pattern, name, ownerType, gameObject, mesh)
      { }

      public Giver(Regex pattern, string name, SensorOwnerType ownerType, GameObject gameObject, MeshRenderer mesh)
        : base(pattern, name, ownerType)
      {
        mGameObject = gameObject;
        mMesh = mesh;
        // Calculate penetrator length based on mesh bounds z
        // mBaseLength = meshRenderer.material.GetFloat("_Length"); 
        mBaseLength = CalculateGiverMeshLength(mMesh);
      }

      public override GameObject GameObject => mGameObject;
      public override bool Enabled { get; set; }
      public override float Value => mValue;

      private static float CalculateGiverMeshLength(MeshRenderer meshRenderer)
      {
        float length = 0f;
        var vertices = meshRenderer.GetComponent<MeshFilter>().sharedMesh.vertices;
        foreach (var vertex in vertices) {
          length = Math.Max(length, vertex.z);
        }
        return length;
      }
    }

    public class DPSPenetrator : Giver
    {
      public static float TipID = 0.09f;

      public Light mLight0;

      public DPSPenetrator(string name, SensorOwnerType ownerType, GameObject gameObject, MeshRenderer mesh, Light light)
        : base(name, ownerType, gameObject, mesh)
      {
        mLight0 = light;
      }

      public override bool Active {
        get {
          if (mLight0 != null) {
            return mLight0.gameObject.activeInHierarchy;
          } else {
            return false;
          }
        }
      }
    }

    public class Taker : Sensor
    {
      public static Regex Pattern => Giver.Pattern; // Giver and Taker use the same pattern, since difference is based on mesh presence or DPS identification

      public float mCumulativeValue = 0f;
      public int mNumPenetrators = 0;

      protected GameObject mGameObject;

      public Taker(string name, SensorOwnerType ownerType, GameObject gameObject)
        : this(Pattern, name, ownerType, gameObject)
      { }

      public Taker(Regex pattern, string name, SensorOwnerType ownerType, GameObject gameObject)
        : base(pattern, name, ownerType)
      {
        mGameObject = gameObject;
      }

      public override GameObject GameObject => mGameObject;
      public override bool Enabled { get; set; }

      public override float Value { 
        get {
          // Taker values are an average of all current givers, for that DP action �
          return mNumPenetrators > 0 ? mCumulativeValue / mNumPenetrators : 0f;
        }
      }
    }

    public class DPSOrifice : Taker
    {
      public static float MiddleID = 0.05f;
      public static float EntranceID_A = 0.01f;
      public static float EntranceID_B = 0.02f;

      public Light mLight1 = null;
      public Light mLight2 = null;

      public DPSOrifice(string name, SensorOwnerType ownerType, GameObject gameObject)
        : base(name, ownerType, gameObject) 
      { }

      public override bool Active {
        get {
          if (mLight1 != null && mLight2 != null) {
            return mLight1.gameObject.activeInHierarchy && mLight2.gameObject.activeInHierarchy;
          } else {
            return false;
          }
        }
      }
    }

    public bool Enabled { get; set; }
    public IEnumerable<Sensor> Sensors => mSensorInstances.Values;
    public int Count => mSensorInstances.Count;
    public event EventHandler<Sensor> SensorDiscovered;
    public event EventHandler<Sensor> SensorLost;

    private Dictionary<int, Sensor> mSensorInstances = new Dictionary<int, Sensor>();
    private Dictionary<int, Giver> mGivers = new Dictionary<int, Giver>();
    private Dictionary<int, Taker> mTakers = new Dictionary<int, Taker>();

    public ThrustVectorProvider()
    {
      VRCHooks.AvatarIsReady += OnAvatarIsReady;
    }

    public void Dispose()
    {
      VRCHooks.AvatarIsReady -= OnAvatarIsReady;
    }

    public void OnSceneWasInitialized()
    {
      foreach (var kv in mSensorInstances) {
        SensorLost?.Invoke(this, kv.Value);
      }
      mSensorInstances.Clear();
      mGivers.Clear();
      mTakers.Clear();

      foreach (var light in GameObject.FindObjectsOfTypeAll(Il2CppType.Of<Light>())) {
        MatchDPSLight(light.Cast<Light>(), null);
      }
      foreach (var gameObject in GameObject.FindObjectsOfTypeAll(Il2CppType.Of<GameObject>())) {
        MatchThrustVector(gameObject.Cast<GameObject>(), null);
      }
    }

    private void OnAvatarIsReady(object sender, VRCPlayer player)
    {
      foreach (var light in player.prop_Player_0.GetComponentsInChildren<Light>(true)) {
        MatchDPSLight(light, player);
      }
      foreach (var gameObject in player.prop_Player_0.GetComponentsInChildren<GameObject>(true)) {
        MatchThrustVector(gameObject, player);
      }
    }

    private void MatchDPSLight(Light light, VRCPlayer player)
    {
      var sensorType = player == null ? SensorOwnerType.World : (player == Util.LocalPlayer ? SensorOwnerType.LocalPlayer : SensorOwnerType.RemotePlayer); 

      if (DPSLightId(light, DPSPenetrator.TipID)) {
        // Step up the hierarchy until we find an object with a mesh as child
        GameObject root;
        for (root = light.gameObject; root != null && root.GetComponentInChildren<MeshRenderer>(true) == null; root = root.transform.parent?.gameObject);
        if (root == null) {
          Error("Discovered a misconfigured Dynamic Penetration System penetrator. Penetrators must have a mesh somewhere in its hierarchy!");
          return;
        }

        var meshRenderer = root.GetComponentInChildren<MeshRenderer>(true);
        var penetrator = new DPSPenetrator(root.name, sensorType, root, meshRenderer, light);
        mGivers[root.GetInstanceID()] = penetrator;
        mSensorInstances[root.GetInstanceID()] = penetrator;
        SensorDiscovered?.Invoke(this, penetrator);
      } else if (DPSLightId(light, DPSOrifice.MiddleID) || DPSLightId(light, DPSOrifice.EntranceID_A) || DPSLightId(light, DPSOrifice.EntranceID_B)) {
        var gameObject = light.gameObject.transform.parent.gameObject;
        DPSOrifice orifice;
        if (!mTakers.ContainsKey(gameObject.GetInstanceID())) {
          orifice = new DPSOrifice(gameObject.name, sensorType, gameObject);
        } else {
          orifice = mTakers[gameObject.GetInstanceID()] as DPSOrifice;
        }
        if (DPSLightId(light, DPSOrifice.MiddleID)) {
          orifice.mLight1 = light;
        } else if (DPSLightId(light, DPSOrifice.EntranceID_A) || DPSLightId(light, DPSOrifice.EntranceID_B)) {
          orifice.mLight2 = light;
        }
        mTakers[gameObject.GetInstanceID()] = orifice;
        mSensorInstances[gameObject.GetInstanceID()] = orifice;
        if (orifice.mLight1 != null && orifice.mLight2 != null) {
          SensorDiscovered?.Invoke(this, orifice);
        }
      }
    }

    private static bool DPSLightId(Light light, float id)
    {
      return light.color.maxColorComponent < 0.01f && Mathf.Abs((light.range % 0.1f) - id) < 0.005f;
    }

    private void MatchThrustVector(GameObject gameObject, VRCPlayer player)
    {
      if (!Giver.Pattern.Match(gameObject.name).Success || !Taker.Pattern.Match(gameObject.name).Success) return;

      // Don't overwrite DPS sensors if user does manual configuration
      if (mSensorInstances.ContainsKey(gameObject.GetInstanceID())) {
        Util.DebugLog($"Skipping ThustVector \"{gameObject.name}\" since it was already claimed by DPS");
        return;
      }

      var sensorType = player == null ? SensorOwnerType.World : (player == Util.LocalPlayer ? SensorOwnerType.LocalPlayer : SensorOwnerType.RemotePlayer); 

      // If object contains mesh, treat it as a penetrator, otherwise orifice
      var meshRenderer = gameObject.transform.GetComponentInChildren<MeshRenderer>(true);
      if (meshRenderer != null) {
        var penetrator = new Giver(gameObject.name, sensorType, gameObject, meshRenderer);
        mGivers[gameObject.GetInstanceID()] = penetrator;
        mSensorInstances[gameObject.GetInstanceID()] = penetrator;
        SensorDiscovered?.Invoke(this, penetrator);
      } else {
        var orifice = new Taker(gameObject.name, sensorType, gameObject);
        mTakers[gameObject.GetInstanceID()] = orifice;
        mSensorInstances[gameObject.GetInstanceID()] = orifice;
        SensorDiscovered?.Invoke(this, orifice);
      }
    }

    public void OnUpdate()
    {
      // Check for destructions
      var lostSensors = new List<int>();
      foreach (var kv in mSensorInstances) {
        if (kv.Value.GameObject == null) {
          lostSensors.Add(kv.Key);
        }
      }
      foreach (var id in lostSensors) {
        var lostSensor = mSensorInstances[id];
        mSensorInstances.Remove(id);
        mGivers.Remove(id);
        mTakers.Remove(id);
        SensorLost?.Invoke(this, lostSensor);
      }

      // Zero sensor values
      foreach (var penetrator in mGivers.Values) {
        penetrator.mValue = 0f;
      }
      foreach (var orifice in mTakers.Values) {
        orifice.mCumulativeValue = 0f;
        orifice.mNumPenetrators = 0;
      }

      // Update penetrations
      foreach (var giver in mGivers.Values) {
        if (!giver.Active) continue;

        var p0 = giver.GameObject.transform.position;
        foreach (var taker in mTakers.Values) {
          if (!taker.Active) continue;

          if (!giver.Enabled && !taker.Enabled) continue;

          float value = 0f;

          // Simple euclidian distance
          {
            var p1 = taker.GameObject.transform.position;
            // TODO: Angle limit?
            float distance = Vector3.Distance(p0, p1) + 0.1f;
            float alpha = Math.Max(0f, Math.Min(1 - distance / giver.Length, 1f));
            if (alpha > 0) {
              value = alpha;
            }
          }

          // Dynamic Penetration System compatibility
          // Sample bezier distance for a rough estimation of the shader calculation
          if (giver is DPSPenetrator && taker is DPSOrifice) {
            var penetrator = giver as DPSPenetrator;
            var orifice = taker as DPSOrifice;

            var p1 = penetrator.mLight0.transform.position;
            var p2 = orifice.mLight1.transform.position;
            var p3 = orifice.mLight2.transform.position;

            // Sample bezier distance for a rough estimation of the Dynamic Penetration System calculation
            float distance = 0f;
            const int samples = 5;
            Vector3 q1 = p0;
            for (int i = 1; i <= samples; i++) {
              float t = i * (1f / samples);
              Vector3 q2 = Mathf.Pow(1 - t, 3) * p0 + 3 * Mathf.Pow(1 - t, 2) * t * p1 + 3 * (1 - t) * Mathf.Pow(t, 2) * p2 + Mathf.Pow(t, 3) * p3;
              distance += Vector3.Distance(q1, q2);
              q1 = q2;
            }
            // Util.DebugLog($"{dpsPenetrator.Name}: {distance} / {dpsPenetrator.Length}");

            float alpha = Math.Max(0f, Math.Min(1 - distance / penetrator.Length, 1f));
            if (alpha > 0) {
              value = alpha;
            }
          }

          if (value > giver.mValue) {
            giver.mValue = value;
            taker.mCumulativeValue += value;
            taker.mNumPenetrators += 1;
          }
        }
      }
    }
  }
}