using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Buttplug;
using UnityEngine;
using UnityEngine.Animations;
using static MelonLoader.MelonLogger;

namespace VibeGoesBrrr
{
  class DeviceSensorBinder
  {
    public Dictionary<ButtplugClientDevice, List<(Sensor Sensor, int? Feature)>> Bindings = new Dictionary<ButtplugClientDevice, List<(Sensor Sensor, int? Feature)>>();
    
    public event EventHandler<(ButtplugClientDevice Device, Sensor Sensor, int? Feature)> BindingAdded;
    public event EventHandler<(ButtplugClientDevice Device, Sensor Sensor, int? Feature)> BindingRemoved;

    static DeviceDB mDB = new DeviceDB();
    ButtplugClient mButtplug;
    List<ISensorProvider> mSensorProviders = new List<ISensorProvider>();

    public void SetButtplugClient(ButtplugClient buttplug)
    {
      if (mButtplug != null) {
        buttplug.DeviceRemoved -= OnDeviceRemoved;
        buttplug.DeviceAdded -= OnDeviceAdded;
        buttplug.ServerDisconnect -= OnButtplugDisconnect;
        OnButtplugDisconnect(buttplug, new EventArgs());
      }

      mButtplug = buttplug;
      buttplug.DeviceAdded += OnDeviceAdded;
      buttplug.DeviceRemoved += OnDeviceRemoved;
      buttplug.ServerDisconnect += OnButtplugDisconnect;
    }

    public void AddSensorProvider(ISensorProvider sensorProvider)
    {
      mSensorProviders.Add(sensorProvider);
      sensorProvider.SensorDiscovered += OnSensorDiscovered;
      sensorProvider.SensorLost += OnSensorLost;
      foreach (var sensor in sensorProvider.Sensors) {
        OnSensorDiscovered(sensorProvider, sensor);
      }
    }

    void OnDeviceAdded(object buttplug, Buttplug.DeviceAddedEventArgs e)
    {
      if (mSensorProviders == null) {
        return;
      }

      foreach (var sensorProvider in mSensorProviders) {
        var matches = Match(sensorProvider.Sensors.ToList(), e.Device);
        if (matches.Count > 0) {
          if (!Bindings.ContainsKey(e.Device)) {
            Bindings[e.Device] = matches;
          } else {
            Bindings[e.Device].AddRange(matches);
          }
          foreach (var binding in matches) {
            BindingAdded?.Invoke(this, (e.Device, binding.Sensor, binding.Feature));
          }
        }
      }
    }

    void OnDeviceRemoved(object buttplug, Buttplug.DeviceRemovedEventArgs e)
    {
      var removedBindings = Bindings.ContainsKey(e.Device) ? Bindings[e.Device] : new List<(Sensor Sensor, int? Feature)>();
      Bindings.Remove(e.Device);
      foreach (var binding in removedBindings) {
        BindingRemoved?.Invoke(this, (e.Device, binding.Sensor, binding.Feature));
      }
    }

    void OnButtplugDisconnect(object buttplug, EventArgs e)
    {
      foreach (var device in Bindings.Keys.ToList()) {
        OnDeviceRemoved(mButtplug, new DeviceRemovedEventArgs(device));
      }
    }

    void OnSensorDiscovered(object sensorProviderObj, Sensor sensor)
    {
      // Exception in IL2CPP-to-Managed trampoline, not passing it to il2cpp: System.InvalidOperationException: Collection was modified; enumeration operation may not execute.
      // at System.ThrowHelper.ThrowInvalidOperationException (System.ExceptionResource resource) [0x0000b] in <e1319b7195c343e79b385cd3aa43f5dc>:0 
      // at System.Collections.Generic.List`1+Enumerator[T].MoveNextRare () [0x00013] in <e1319b7195c343e79b385cd3aa43f5dc>:0 
      // at System.Collections.Generic.List`1+Enumerator[T].MoveNext () [0x0004a] in <e1319b7195c343e79b385cd3aa43f5dc>:0 
      // at VibeGoesBrrr.DeviceSensorBinder.OnSensorDiscovered (System.Object sensorProviderObj, VibeGoesBrrr.Sensor sensor) [0x000a9] in <f8df15c91b9644ada170c0a2cfbb8f24>:0 
      // at (wrapper delegate-invoke) System.EventHandler`1[VibeGoesBrrr.Sensor].invoke_void_object_TEventArgs(object,VibeGoesBrrr.Sensor)
      // at VibeGoesBrrr.ThrustVectorProvider.MatchDPSLight (UnityEngine.Light light, VRCPlayer player) [0x000f8] in <f8df15c91b9644ada170c0a2cfbb8f24>:0 
      // at VibeGoesBrrr.ThrustVectorProvider.OnAvatarIsReady (System.Object sender, VRCPlayer player) [0x0001b] in <f8df15c91b9644ada170c0a2cfbb8f24>:0 
      // at (wrapper delegate-invoke) System.EventHandler`1[VRCPlayer].invoke_void_object_TEventArgs(object,VRCPlayer)
      // at VibeGoesBrrr.VRCHooks+<>c__DisplayClass11_0.<OnVRCPlayerAwake>b__0 () [0x00022] in <f8df15c91b9644ada170c0a2cfbb8f24>:0 
      // at (wrapper dynamic-method) UnhollowerRuntimeLib.DelegateSupport.(il2cpp delegate trampoline) System.Void(intptr,UnhollowerBaseLib.Runtime.Il2CppMethodInfo*)
      if (mButtplug != null) {
        foreach (var device in mButtplug.Devices) {
          var matches = Match(new List<Sensor> { sensor }, device);
          if (matches.Count > 0) {
            if (!Bindings.ContainsKey(device)) {
              Bindings[device] = matches;
            } else {
              Bindings[device].AddRange(matches);
            }
            foreach (var binding in matches) {
              BindingAdded?.Invoke(this, (device, binding.Sensor, binding.Feature));
            }
          }
        }
      }
    }

    void OnSensorLost(object sensorManager, Sensor sensor)
    {
      foreach (var kv in Bindings) {
        var removedBindings = kv.Value.FindAll(binding => binding.Sensor == sensor);
        kv.Value.RemoveAll(binding => binding.Sensor == sensor);
        foreach (var binding in removedBindings) {
          BindingRemoved?.Invoke(this, (kv.Key, binding.Sensor, binding.Feature));
        }
      }
    }

    static List<(Sensor Sensor, int? Feature)> Match(List<Sensor> sensors, ButtplugClientDevice device)
    {
      var matching = new List<(Sensor, int?)>();
      foreach (var sensor in sensors) {
        // Auto-assign thrust vector sensors without tags to certain toys, to make them work "out of the box"
        if (string.IsNullOrWhiteSpace(sensor.Tag) && (sensor is ThrustVectorProvider.Giver || sensor is ThrustVectorProvider.Taker)) {
          // Only auto attach to specific bone types (so we don't end up with vibrating mouth givers etc)
          HumanBodyBones[] validBoneParents = { HumanBodyBones.Spine, HumanBodyBones.Hips, HumanBodyBones.Chest, HumanBodyBones.UpperChest };
          if (IsChildOfBoneType(sensor.GameObject.transform, validBoneParents)) {
            var iostDevice = mDB.FindDevice(device.Name);
            if (iostDevice != null) {
              if (sensor is ThrustVectorProvider.Giver && iostDevice.IsGiver) {
                matching.Add((sensor, null));
                continue;
              } else if (sensor is ThrustVectorProvider.Taker && iostDevice.IsTaker) {
                matching.Add((sensor, null));
                continue;
              }
            }
          }
        }
        
        // HapticsSensor compatibility
        if (sensor is TouchZoneProvider.TouchSensor && Regex.Match(sensor.Type, @"HapticsSensor_", RegexOptions.IgnoreCase).Success) {
          try {
            int index = int.Parse(sensor.Tag.Substring(0, 1));
            if (index == device.Index) {
              matching.Add((sensor, null));
              continue;
            }
          } catch (System.FormatException) {
            Error($"Misconfigured sensor \"{sensor.Name}\". Please refer to the instructions on how to set up and name Touch Zones.");
          }
        }

        // Assign untagged sensors to all devices for convenience
        if (string.IsNullOrWhiteSpace(sensor.Tag) && !(sensor is ThrustVectorProvider.Giver || sensor is ThrustVectorProvider.Taker)) { // ThrustVectors without tags are auto-assigned a single device instead of all
          matching.Add((sensor, null));
          continue;
        }

        // General matching based on standard tag format
        var binding = MatchDevice(sensor.Tag, sensor, device);
        if (binding != null) {
          matching.Add(binding.Value);
        } 
      }
      return matching;
    }

    static (Sensor Sensor, int? Feature)? MatchDevice(string needle, Sensor sensor, ButtplugClientDevice device)
    {
      if (string.IsNullOrWhiteSpace(needle)) {
        return null;
      }

      var match = Regex.Match(needle, @"^(.+?)?\s*(?:#(\d+?))?\s*$", RegexOptions.IgnoreCase);
      if (match.Success) {
        // Partial match on device name
        if (match.Groups[1].Success && !device.Name.ToLower().Contains(match.Groups[1].Value.ToLower())) {
          return null;
        }

        // Motor indices are 1-based
        // null index = all motors
        int? motorIndex = null;
        if (device.AllowedMessages.ContainsKey(Buttplug.ServerMessage.Types.MessageAttributeType.VibrateCmd)) {
          var motorCount = device.AllowedMessages[Buttplug.ServerMessage.Types.MessageAttributeType.VibrateCmd].FeatureCount;
          motorIndex = match.Groups[2].Success ? (int?)int.Parse(match.Groups[2].Value) - 1 : null;
          if (motorIndex != null && ((int)motorIndex < 0 || (int)motorIndex >= motorCount)) {
            Error($"Invalid motor index \"{motorIndex + 1}\" for sensor \"{sensor.Name}\". The device \"{device.Name}\" reports only having {motorCount} motor{(motorCount > 0 ? "s" : "")}. Make sure you set up your sensor names correctly.");
            return null;
          }
        }

        return (sensor, motorIndex);
      } else {
        return null;
      }
    }

    static bool IsChildOfBoneType(Transform transform, HumanBodyBones[] boneTypes)
    {
      var animator = transform.GetComponentInParent<Animator>();
      if (animator == null) {
        return false;
      }

      // Create a mapping from bone -> HumanBodyBones since Unity bizzarrely doesn't provide this
      var boneIds = new Dictionary<int, HumanBodyBones>();
      foreach (HumanBodyBones boneId in Enum.GetValues(typeof(HumanBodyBones))) {
        if (boneId == HumanBodyBones.LastBone) {
          continue;
        }
        var boneTransform = animator.GetBoneTransform(boneId);
        if (boneTransform != null) {
          boneIds[boneTransform.GetInstanceID()] = boneId;
        }
      }

      // Figure out what's the first parent bone of sensor
      for (Transform parent = transform; parent != null; parent = parent.transform.parent) {
        if (boneIds.ContainsKey(parent.GetInstanceID())) {
          // If the bone is whitelisted, we're good
          var boneId = boneIds[parent.GetInstanceID()];
          if (boneTypes.Contains(boneId)) {
            return true;
          } else {
            break;
          }
        }
      }

      // If no bone parent was found, check for a parent constraint instead
      var constraint = transform.GetComponentInParent<ParentConstraint>();
      if (constraint != null) {
        for (int i = 0; i < constraint.sourceCount; i++) {
          var source = constraint.GetSource(i);
          if (boneIds.ContainsKey(source.sourceTransform.GetInstanceID())) {
            var boneId = boneIds[source.sourceTransform.GetInstanceID()];
            if (boneTypes.Contains(boneId)) {
              return true;
            }
          }
        }
      }

      return false;
    }
  }
}
