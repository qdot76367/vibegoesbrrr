﻿using Buttplug;
using Harmony;
using MelonLoader;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using UnhollowerRuntimeLib;
using UnityEngine;
using static MelonLoader.MelonLogger;

namespace VibeGoesBrrr
{
  public static class BuildInfo
  {
    public const string Name = "VibeGoesBrrr";
    public const string Author = "Jace";
    public const string Version = "0.4.4";
    public const string DownloadLink = "https://gum.co/VibeGoesBrrr";
  }

  static public class NativeMethods
  {
      public static string TempPath {
        get {
          string tempPath = Path.Combine(Path.GetTempPath(), $"{BuildInfo.Name}-{BuildInfo.Version}");
          if (!Directory.Exists(tempPath)) {
            Directory.CreateDirectory(tempPath);
          }
          return tempPath;
        }
      }

      [DllImport("kernel32.dll")]
      public static extern IntPtr LoadLibrary(string dllToLoad);

      public static string LoadUnmanagedLibraryFromResource(Assembly assembly, string libraryResourceName, string libraryName)
      {
          string assemblyPath = Path.Combine(TempPath, libraryName);

          Msg($"Unpacking and loading {libraryName}");
          #if DEBUG
            try{
          #endif
              using (Stream s = assembly.GetManifestResourceStream(libraryResourceName)) {
                  var data = new BinaryReader(s).ReadBytes((int)s.Length);
                  File.WriteAllBytes(assemblyPath, data);
              }
          #if DEBUG
            } catch (System.IO.IOException) { }
          #endif
          NativeMethods.LoadLibrary(assemblyPath);

          return assemblyPath;
      }
  }

  public class VibeGoesBrrrMod : MelonMod
  {
    private bool mTouchEnabled = true;
    private bool mThrustEnabled = true;
    private bool mTouchFeedbackEnabled = true;
    private bool mIdleEnabled = false;
    private bool mExpressionParametersEnabled = true;
    private float mIdleIntensity = 5f;
    private float mMaxIntensity = 100f;
    private float mIntensityCurveExponent = 3f;
    private string mServerURI = "ws://localhost:12345";
    private float mUpdateFreq = 10f;
    private float mScanDuration = 15f;
    private float mScanWaitDuration = 5f;
    private bool mSetupMode = false;

    private Harmony.HarmonyInstance mHarmony;
    private AssetBundle mBundle;
    private Shader mShader;
    private GameObject mOrthographicFrustum;
    private Process mIntifaceServer;
    private ButtplugClient mButtplug = null;
    private Task mConnectionTask = Task.CompletedTask;
    private Task mScanTask = Task.CompletedTask;
    private float mNextUpdate = 0;
    private int mMaxSeenDevices = 0;
    private Dictionary<uint, float[]> mDeviceIntensities = new Dictionary<uint, float[]>();
    private HashSet<Sensor> mTouchAndThrustSensors = new HashSet<Sensor>();
    private HashSet<Sensor> mFeedbackSensors = new HashSet<Sensor>();
    private HashSet<Sensor> mExpressionSensors = new HashSet<Sensor>();
    private TouchZoneProvider mSensorManager;
    private ThrustVectorProvider mThrustVectorManager;
    private DeviceSensorBinder mBinder;
    private ExpressionParam<float> mGlobalParam;

    public override void OnApplicationStart()
    {
      _ = UpdateCheck();

      // Load Buttplug native library from resources
      NativeMethods.LoadUnmanagedLibraryFromResource(Assembly.GetExecutingAssembly(), "buttplug_rs_ffi.dll", "buttplug_rs_ffi.dll");

      ClassInjector.RegisterTypeInIl2Cpp<OrthographicDepth>();
      ClassInjector.RegisterTypeInIl2Cpp<OrthographicFrustum>();

      MelonPreferences.CreateCategory(BuildInfo.Name, "Vibe Goes Brrr~");
      MelonPreferences.CreateEntry(BuildInfo.Name, "TouchEnabled", mTouchEnabled, "Touch Vibrations");
      MelonPreferences.CreateEntry(BuildInfo.Name, "ThrustEnabled", mThrustEnabled, "Thrust Vibrations");
      MelonPreferences.CreateEntry(BuildInfo.Name, "TouchFeedbackEnabled", mTouchFeedbackEnabled, "Touch Feedback");
      MelonPreferences.CreateEntry(BuildInfo.Name, "IdleEnabled", mIdleEnabled, "Idle Vibrations");
      MelonPreferences.SetEntryValue(BuildInfo.Name, "IdleEnabled", false);
      MelonPreferences.CreateEntry(BuildInfo.Name, "IdleIntensity", mIdleIntensity, "Idle Vibration Intensity %");
      MelonPreferences.CreateEntry(BuildInfo.Name, "MaxIntensity", mMaxIntensity, "Max Vibration Intensity %");
      MelonPreferences.CreateEntry(BuildInfo.Name, "ExpressionParametersEnabled", mExpressionParametersEnabled, "Expression Parameters");
      MelonPreferences.CreateEntry(BuildInfo.Name, "SetupMode", mSetupMode, "Setup Mode");
      if (!Util.Debug) {
        MelonPreferences.SetEntryValue(BuildInfo.Name, "SetupMode", false);
      }
      MelonPreferences.CreateEntry(BuildInfo.Name, "RestartIntiface", false, "Restart Intiface");
      MelonPreferences.SetEntryValue(BuildInfo.Name, "RestartIntiface", false);
      MelonPreferences.CreateEntry(BuildInfo.Name, "UpdateFreq", mUpdateFreq, "Update Frequency");
      // Hidden preferences
      MelonPreferences.CreateEntry(BuildInfo.Name, "ServerURI", mServerURI, "Server URI", is_hidden: true);
      MelonPreferences.CreateEntry(BuildInfo.Name, "ScanDuration2", mScanDuration, "Scan Duration", is_hidden: true);
      MelonPreferences.CreateEntry(BuildInfo.Name, "ScanWaitDuration2", mScanWaitDuration, "Scan Wait Duration", is_hidden: true);
      MelonPreferences.CreateEntry(BuildInfo.Name, "IntensityCurveExponent", mIntensityCurveExponent, "Intensity Curve Exponent", is_hidden: true);
      OnPreferencesSaved();

      mHarmony = HarmonyInstance.Create(BuildInfo.Name);
      VRCHooks.OnApplicationStart(mHarmony);
      VRCHooks.AvatarIsReady += OnAvatarIsReady;

      mSensorManager = new TouchZoneProvider();
      mSensorManager.SensorDiscovered += OnSensorDiscovered;
      mSensorManager.SensorLost += OnSensorLost;

      mThrustVectorManager = new ThrustVectorProvider();
      mThrustVectorManager.SensorDiscovered += OnSensorDiscovered;
      mThrustVectorManager.SensorLost += OnSensorLost;

      mBinder = new DeviceSensorBinder();
      mBinder.BindingAdded += (_, e) => {
        if (e.Sensor.OwnerType == SensorOwnerType.LocalPlayer) {
          // Send a lil' doot-doot 🎺 when successfully bound!
          DootDoot(e.Device);
          Msg($"Device \"{e.Device.Name}\" bound to sensor \"{e.Sensor.Name}\"");
        } else if (Util.Debug) {
          Util.DebugLog($"Device \"{e.Device.Name}\" bound to sensor \"{e.Sensor.Name}\"");
        }
      };
      mBinder.BindingRemoved += (_, e) => {
        if (e.Sensor.OwnerType == SensorOwnerType.LocalPlayer) {
          Msg($"Device \"{e.Device.Name}\" unbound from sensor \"{e.Sensor.Name}\"");
        } else if (Util.Debug) {
          Util.DebugLog($"Device \"{e.Device.Name}\" unbound from sensor \"{e.Sensor.Name}\"");
        }
        _ = e.Device?.SendStopDeviceCmd();
      };
      mBinder.AddSensorProvider(mSensorManager);
      mBinder.AddSensorProvider(mThrustVectorManager);

      mConnectionTask = InitializeButtplugClient();
      LoadAssets();
    }

    public override void OnApplicationQuit()
    {
      _ = mButtplug?.DisconnectAsync();
    }

    private async Task InitializeButtplugClient()
    {
      if (mButtplug != null) {
        mButtplug.Dispose();
      }

      mButtplug = new ButtplugClient(BuildInfo.Name);

      mButtplug.ServerDisconnect += (sender, e) => {
        Warning($"Lost connection to Buttplug server! Attempting to reconnect...");
      };

      mButtplug.DeviceAdded += (sender, e) => {
        mMaxSeenDevices = Math.Max(mMaxSeenDevices, mButtplug.Devices.Length);

        var message = $"Device \"{e.Device.Name}\" connected";
        var supporting = new List<string>();
        try {
          var motorCount = e.Device.AllowedMessages[Buttplug.ServerMessage.Types.MessageAttributeType.VibrateCmd].FeatureCount;
          if (motorCount > 0) {
            supporting.Add($"{motorCount} vibration motor{(motorCount > 1 ? "s" : "")}");
            mDeviceIntensities[e.Device.Index] = new float[motorCount];
          }
        } catch (KeyNotFoundException) { }
        try {
          var rotatorCount = e.Device.AllowedMessages[Buttplug.ServerMessage.Types.MessageAttributeType.RotateCmd].FeatureCount;
          if (rotatorCount > 0) {
            supporting.Add($"{rotatorCount} rotation actuator{(rotatorCount > 1 ? "s" : "")}");
          }
        } catch (KeyNotFoundException) { }
        if (e.Device.Name.Contains("Lovense Max")) {
          supporting.Add("pump");
        }
        if (supporting.Count > 0) {
          message += ", supporting " + String.Join(", ", supporting) + ".";
        }
        Msg(message);
#if DEBUG
        Msg($"{e.Device.Name} supports the following messages:");
        foreach (var msgInfo in e.Device.AllowedMessages) {
          Msg($"- {msgInfo.Key.ToString()}");
          if (msgInfo.Value.FeatureCount > 0) {
            Msg($"  - Feature Count: {msgInfo.Value.FeatureCount}");
          }
        }
#endif
      };

      mButtplug.DeviceRemoved += (sender, e) => {
        mDeviceIntensities.Remove(e.Device.Index);
        Msg($"Device \"{e.Device.Name}\" disconnected");
      };

      mButtplug.ErrorReceived += (sender, e) => {
        Warning($"Device error: {e.Exception.Message}");
      };
      
      mButtplug.SorterCallbackException += (sender, e) => {
        Error(e);
        Error($"Critical Buttplug client exception. Reinitializing...");
      };

      mBinder.SetButtplugClient(mButtplug);

      await ConnectButtplugClient();
    }

    async Task ConnectButtplugClient()
    {
      if (mButtplug.Connected) {
        Util.DebugLog("Disconnecting...");
        await mButtplug.DisconnectAsync();
      }

      var conn = new ButtplugWebsocketConnectorOptions(new System.Uri(mServerURI));
      try {
        Util.DebugLog("Connecting...");
        await mButtplug.ConnectAsync(conn);
        if (mIntifaceServer == null) {
          Warning("Connected to Intiface Desktop server. It's recommended not to have Intiface Desktop running, so Vibe Goes Brrr is able to start and manage the server automatically and restart it if something goes wrong. Only run Intiface Desktop manually if you know what you're doing.");
        }
      } catch (ButtplugConnectorException) {
        Msg($"Starting embedded Intiface server...");
        try {
          StartIntifaceServer();
        } catch (Exception e) {
          Error($"Failed to start Intiface server. Is Intiface Desktop properly installed?");
          Error(e.Message);
          return;
        }
        Util.DebugLog("Connecting to embedded...");
        try {
          await mButtplug.ConnectAsync(conn);
        } catch (Exception e) {
          Error(e);
          Error("Failed to connect to embedded server. RIP.");
        }
      }

      Msg($"Connected to Buttplug server at {mServerURI}");
    }

    void LoadAssets()
    {
#if DEBUG
      Msg("Resources:");
      foreach (var res in Assembly.GetExecutingAssembly().GetManifestResourceNames()) {
        Msg($"- {res}");
      }
#endif

      var assetStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("assetbundle");
      if (assetStream == null) {
        Error("Failed to load asset stream");
      }
      var tempStream = new MemoryStream((int)assetStream.Length);
      if (tempStream == null) {
        Error("Failed to load temp stream");
      }
      assetStream.CopyTo(tempStream);
      mBundle = AssetBundle.LoadFromMemory_Internal(tempStream.ToArray(), 0);
      if (mBundle == null) {
        Error("Failed to load asset bundle");
      }
      mBundle.hideFlags |= HideFlags.DontUnloadUnusedAsset;

#if DEBUG
      foreach (var name in mBundle.GetAllAssetNames()) {
        Msg($"Asset: {name}");
      }
#endif
      mShader = mBundle.LoadAsset_Internal("Assets/VibeGoesBrrr Internal/OrthographicDepth.shader", Il2CppType.Of<Shader>()).Cast<Shader>();
      if (!mShader) {
        Error("Failed to load shader");
      }
      mShader.hideFlags |= HideFlags.DontUnloadUnusedAsset;
      TouchZoneProvider.TouchSensor.Shader = mShader;
      var orthographicFrustumMat = mBundle.LoadAsset_Internal("Assets/VibeGoesBrrr Internal/OrthographicFrustum.mat", Il2CppType.Of<Material>()).Cast<Material>();
      if (!orthographicFrustumMat) {
        Error("Failed to load OrthographicFrustum material");
      }
      orthographicFrustumMat.hideFlags |= HideFlags.DontUnloadUnusedAsset;
      mOrthographicFrustum = mBundle.LoadAsset_Internal("Assets/VibeGoesBrrr Internal/OrthographicFrustum.prefab", Il2CppType.Of<GameObject>()).Cast<GameObject>();
      if (!mOrthographicFrustum) {
        Error("Failed to load OrthographicFrustum prefab");
      }
      mOrthographicFrustum.hideFlags |= HideFlags.DontUnloadUnusedAsset;
      mBundle.LoadAllAssets();
    }

    private async Task UpdateCheck()
    {
      // Check for VibeGoesBrrr~ updates
      try {
        var req = WebRequest.Create(@"https://gitlab.com/api/v4/projects/22979183/releases");
        var response = (HttpWebResponse)(await req.GetResponseAsync());
        var json = new StreamReader(response.GetResponseStream()).ReadToEnd();
        var releases = JArray.Parse(json).OrderByDescending(t => t["commit"]["created_at"]).ToList();
        if (releases.Count == 0) {
          return;
        }
        var latest = new Version(releases[0]["tag_name"].ToString().Replace("v", ""));
        var current = new Version(BuildInfo.Version);
        bool isPrerelease = latest < current || Assembly.GetName().Name.Contains("-pre");
        if (latest > current || (latest == current && isPrerelease)) {
          if (isPrerelease) {
            Error("You are running an outdated prerelease version!");
          }
          Warning("A new version of Vibe Goes Brrr~ is avaliable!");
          var links = releases[0]["assets"]["links"];
          if (links.HasValues) {
            Warning($"{links[0]["url"]} ({links[0]["name"]})");
          }
          foreach (var release in releases) {
            var version = Version.Parse(release["tag_name"].ToString().Replace("v", ""));
            if (version <= current && !(version == current && isPrerelease)) {
              break;
            }
            if (release["description"].ToString() != "") {
              Warning("");
              Warning($"v{version}:");
              foreach (var line in release["description"].ToString().Split('\n')) {
                Warning(line);
              }
            }
          }
        } else if (isPrerelease) {
          Warning($"Version {current.ToString(3)} (pre-release)");
        }
      } catch (Exception) {
        Msg("Update check failed.");
      }

      // Check for Intiface Engine updates
      try {
        var req = (HttpWebRequest)WebRequest.Create(@"https://api.github.com/repos/intiface/intiface-cli-rs/releases");
        req.UserAgent = BuildInfo.Name;
        var response = (HttpWebResponse)(await req.GetResponseAsync());
        var json = new StreamReader(response.GetResponseStream()).ReadToEnd();
        var releases = JArray.Parse(json).OrderByDescending(t => t["created_at"]).ToList();

        var intifaceConfigDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "IntifaceDesktop");
        var intifaceConfig = File.ReadAllText(Path.Combine(intifaceConfigDir, "intiface.config.json"));
        var config = JObject.Parse(intifaceConfig);

        var latestVersion = releases[0]["tag_name"].ToString();
        var currentVersion = config["currentEngineVersion"].ToString();

        Msg($"Intiface Engine {currentVersion}");
        if (currentVersion != latestVersion) {
          Warning($"A new version ({latestVersion}) of Intiface Engine is avaliable!");
          Warning("Open Intiface Desktop and update to get the latest fixes and device features!");
        }
      } catch (Exception) {
        Msg("Intiface Engine update check failed.");
      }
    }

    public override void OnSceneWasInitialized(int buildIndex, string sceneName)
    {
      mSensorManager.OnSceneWasInitialized();
      mThrustVectorManager.OnSceneWasInitialized();
      _ = mButtplug?.StopAllDevicesAsync();
    }

    void OnSensorDiscovered(object _, Sensor sensor)
    {
      // Sensors
      if (sensor.OwnerType == SensorOwnerType.LocalPlayer) {
        mTouchAndThrustSensors.Add(sensor);
        mExpressionSensors.Add(sensor);
        Msg($"Discovered sensor \"{sensor.Name}\"");
      }
      if (sensor.OwnerType == SensorOwnerType.RemotePlayer) {
        mFeedbackSensors.Add(sensor);
        // mExpressionSensors.Add(sensor);
      }
      if (Util.Debug && sensor.OwnerType == SensorOwnerType.World) {
        mTouchAndThrustSensors.Add(sensor);
        mFeedbackSensors.Add(sensor);
        mExpressionSensors.Add(sensor);
      }

      // Frustums
      if (mSetupMode) {
        if (sensor.OwnerType == SensorOwnerType.LocalPlayer || Util.Debug) {
          AddFrustumIfMissing(sensor);
        }
      }

      Util.DebugLog($"Discovered sensor \"{sensor.Name}\" in {sensor.OwnerType.ToString()}");
    }

    void OnSensorLost(object _, Sensor sensor) 
    {
      mTouchAndThrustSensors.Remove(sensor);
      mExpressionSensors.Remove(sensor);
      mFeedbackSensors.Remove(sensor);
      Util.DebugLog($"Lost sensor {sensor.Name} in {sensor.OwnerType.ToString()}");
    }

    void OnAvatarIsReady(object _, VRCPlayer player)
    {
      if (player == Util.LocalPlayer) {
        var param = new ExpressionParam<float>("Intensity");
        if (!param.Valid) {
          param = new ExpressionParam<float>("TouchZone");
          if (!param.Valid) {
            param = new ExpressionParam<float>("VibeSensor");
          }
        }
        mGlobalParam = param.Valid ? param : null;
      }
    }

    private async Task Scan() 
    {
      // Util.DebugLog("Starting scan...");
      await mButtplug.StartScanningAsync();
      await Task.Delay((int)(mScanDuration * 1000));
      // Util.DebugLog("Stopping scan.");
      await mButtplug.StopScanningAsync();
      await Task.Delay((int)(mScanWaitDuration * 1000));
    }

    public override void OnUpdate()
    {
      mSensorManager.OnUpdate();

      if (Util.LocalPlayer == null) return;
      if (mButtplug == null) return;

      if (!mButtplug.Connected) {
        if (mConnectionTask.IsCompleted) {
          mConnectionTask = InitializeButtplugClient();
        }
        return;
      }

      // Scan forever!!!
      if (mScanTask.IsCompleted) {
        mScanTask = Scan();
      }

      if (Time.time < mNextUpdate) return;

      mThrustVectorManager.OnUpdate();

      var activeSensors = new HashSet<Sensor>();
      var deviceIntensities = new Dictionary<uint, float?[]>();
      foreach (var device in mButtplug.Devices) {
        if (!device.AllowedMessages.ContainsKey(Buttplug.ServerMessage.Types.MessageAttributeType.VibrateCmd)) continue;
        uint motorCount = device.AllowedMessages[Buttplug.ServerMessage.Types.MessageAttributeType.VibrateCmd].FeatureCount;
        deviceIntensities[device.Index] = new float?[motorCount];
      }

      // Idle intensities
      if (mIdleEnabled) {
        foreach (var device in mButtplug.Devices) {
          if (!deviceIntensities.ContainsKey(device.Index)) continue;
          var motorIntensities = deviceIntensities[device.Index];
          for (int motorIndex = 0; motorIndex < motorIntensities.Length; motorIndex++) {
            motorIntensities[motorIndex] = Mathf.Clamp(mIdleIntensity / 100, 0, 1);
          }
        }
      }

      // Calculate sensor intensities
      if (mTouchEnabled || mThrustEnabled) {
        foreach (var kv in mBinder.Bindings) {
          var device = kv.Key;
          var bindings = kv.Value;
          if (!deviceIntensities.ContainsKey(device.Index)) continue;

          var motorIntensities = deviceIntensities[device.Index];
          foreach (var (sensor, featureIndex) in bindings) {
            if (!mTouchAndThrustSensors.Contains(sensor)) continue;
            if (Util.Debug && !mSetupMode && sensor.OwnerType == SensorOwnerType.World) continue;
            if (!sensor.Active) continue;

            if (!mTouchEnabled && sensor is TouchZoneProvider.TouchSensor) continue;
            if (!mThrustEnabled && (sensor is ThrustVectorProvider.Giver || sensor is ThrustVectorProvider.Taker)) continue;

            sensor.Enabled = true;

            // Calculate intensity~
            Func<int, double> intensity = (int motorIndex) => {
              float clampedValue = Math.Max(0f, Math.Min(sensor.Value, 1f));
              float intensityCurve = 1f - (float)Math.Pow(1f - clampedValue, mIntensityCurveExponent);
              // Subtract potential idle intensity so the sensor alpha acts fully in the remaining range
              return (motorIntensities[motorIndex] ?? 0f) + intensityCurve * Math.Max(0f, Math.Min(mMaxIntensity / 100f - (motorIntensities[motorIndex] ?? 0f), 1f)); 
            };

            // Only vibrate at the maximum intensity of all accumulated affected sensors
            if (featureIndex != null) {
              int motorIndex = (int)featureIndex;
              motorIntensities[motorIndex] = (float)Math.Max(motorIntensities[motorIndex] ?? 0, intensity(motorIndex));
            } else {
              // Vibrate all motors for unindexed sensors
              for (int motorIndex = 0; motorIndex < motorIntensities.Length; motorIndex++) {
                motorIntensities[motorIndex] = (float)Math.Max(motorIntensities[motorIndex] ?? 0, intensity(motorIndex));
              }
            }

            activeSensors.Add(sensor);
          }
        }
      }

      // Send device commands
      foreach (var device in mButtplug.Devices) {
        if (!deviceIntensities.ContainsKey(device.Index)) continue;

        // Refrain from updating with the same values, since this seems to increase the chance of device hangs
        var motorIntensityValues = Array.ConvertAll(deviceIntensities[device.Index], i => i ?? 0);
        if (!motorIntensityValues.SequenceEqual(mDeviceIntensities[device.Index])) { 
          // VIBRATE!!!
          _ = device.SendVibrateCmd(Array.ConvertAll(motorIntensityValues, i => (double)i));
          mDeviceIntensities[device.Index] = motorIntensityValues;
        }
      }

      // Write expression parameters
      if (mExpressionParametersEnabled) {
        float expressionMax = 0f;

        // Sensor parameter intensities
        foreach (var sensor in mExpressionSensors) {
          if (!sensor.Active) continue;

          sensor.Enabled = true;

          if (sensor.Param != null) {
            sensor.Param.Prioritized = true;
            sensor.Param.Value = sensor.Value;
          }

          if (sensor.OwnerType == SensorOwnerType.LocalPlayer) {
            expressionMax = Mathf.Max(expressionMax, sensor.Value);
          }

          activeSensors.Add(sensor);
        }

        // Global parameter intensity
        if (mGlobalParam != null) {
          mGlobalParam.Prioritized = true;
          mGlobalParam.Value = expressionMax;
        }
      }

      // Calculate and send touch feedback
      if (mTouchFeedbackEnabled) {
        foreach (var sensor in mFeedbackSensors) {
          if (!sensor.Active) continue;

          float minDistance = 0f;

          if (sensor is TouchZoneProvider.TouchSensor) {
            var touchSensor = sensor as TouchZoneProvider.TouchSensor;
            minDistance = touchSensor.Camera.orthographicSize * 4;
          } else if (sensor is ThrustVectorProvider.Giver) {
            var giver = sensor as ThrustVectorProvider.Giver;
            minDistance = giver.Length;
          } else {
            continue;
          }

          var leftDistance = float.MaxValue;
          var leftHand = Util.LocalPlayer.prop_VRCPlayerApi_0.GetBoneTransform(HumanBodyBones.LeftHand);
          if (leftHand != null) {
            leftDistance = Vector3.Distance(sensor.GameObject.transform.position, leftHand.position);
          }
          var rightDistance = float.MaxValue;
          var rightHand = Util.LocalPlayer.prop_VRCPlayerApi_0.GetBoneTransform(HumanBodyBones.RightHand);
          if (rightHand != null) {
            rightDistance = Vector3.Distance(sensor.GameObject.transform.position, rightHand.position);
          }

          if (leftDistance <= minDistance || rightDistance <= minDistance) {
            if (sensor is TouchZoneProvider.TouchSensor) {
              var touchSensor = sensor as TouchZoneProvider.TouchSensor;
              if (Util.Debug && mSetupMode) {
                touchSensor.Camera.cullingMask |= 1 << 10; // PlayerLocal
              } else {
                touchSensor.Camera.cullingMask = 1 << 10; 
              }
            }

            sensor.Enabled = true;

            float intensity = sensor.Value;
            if (intensity > 0f) {
              if (leftDistance <= minDistance) {
                Util.LocalPlayer.prop_VRCPlayerApi_0.PlayHapticEventInHand(VRC.SDKBase.VRC_Pickup.PickupHand.Left, 0.5f, 1f, 5 + 20 * intensity);
              }

              if (rightDistance <= minDistance) {
                Util.LocalPlayer.prop_VRCPlayerApi_0.PlayHapticEventInHand(VRC.SDKBase.VRC_Pickup.PickupHand.Right, 0.5f, 1f, 5 + 20 * intensity);
              }
            }

            activeSensors.Add(sensor);
          }
        }
      }

      // Disable inactive sensors, for performance
      foreach (var sensor in mSensorManager.Sensors) {
        if (!activeSensors.Contains(sensor)) {
          sensor.Enabled = false;
        }
      }

      mNextUpdate = Time.time + (1 / mUpdateFreq);
    }

    private OrthographicFrustum AddFrustumIfMissing(Sensor sensor)
    {
      if (sensor is TouchZoneProvider.TouchSensor) {
        var frustum = sensor.GameObject.GetComponentInChildren<OrthographicFrustum>();
        if (frustum == null) {
          var obj = GameObject.Instantiate(mOrthographicFrustum, sensor.GameObject.transform);
          obj.AddComponent<OrthographicFrustum>();
          Util.DebugLog($"Added debug frustum to \"{sensor.Name}\" sensor");
        }
        return frustum;
      }

      return null;
    }

    public override void OnPreferencesSaved()
    {
      bool touchEnabled = MelonPreferences.GetEntryValue<bool>(BuildInfo.Name, "TouchEnabled");
      if (mTouchEnabled && !touchEnabled) {
        ResetSensorParameterValues();
      }
      mTouchEnabled = touchEnabled;

      bool thrustEnabled = MelonPreferences.GetEntryValue<bool>(BuildInfo.Name, "ThrustEnabled");
      if (mThrustEnabled && !thrustEnabled) {
        ResetSensorParameterValues();
      }
      mThrustEnabled = thrustEnabled;

      var idleEnabled = MelonPreferences.GetEntryValue<bool>(BuildInfo.Name, "IdleEnabled");
      if (mIdleEnabled && !idleEnabled) {
        ResetSensorParameterValues();
      }
      mIdleEnabled = idleEnabled;

      var expressionParametersEnabled = MelonPreferences.GetEntryValue<bool>(BuildInfo.Name, "ExpressionParametersEnabled");
      if (mExpressionParametersEnabled && !expressionParametersEnabled) {
        ResetSensorParameterValues();
      }
      mExpressionParametersEnabled = expressionParametersEnabled;

      mTouchFeedbackEnabled = MelonPreferences.GetEntryValue<bool>(BuildInfo.Name, "TouchFeedbackEnabled");
      mIdleIntensity = MelonPreferences.GetEntryValue<float>(BuildInfo.Name, "IdleIntensity");
      mMaxIntensity = MelonPreferences.GetEntryValue<float>(BuildInfo.Name, "MaxIntensity");
      mServerURI = MelonPreferences.GetEntryValue<string>(BuildInfo.Name, "ServerURI");
      mUpdateFreq = MelonPreferences.GetEntryValue<float>(BuildInfo.Name, "UpdateFreq");
      mScanDuration = MelonPreferences.GetEntryValue<float>(BuildInfo.Name, "ScanDuration2");
      mScanWaitDuration = MelonPreferences.GetEntryValue<float>(BuildInfo.Name, "ScanWaitDuration2");
      mIntensityCurveExponent = MelonPreferences.GetEntryValue<float>(BuildInfo.Name, "IntensityCurveExponent");

      bool setupMode = MelonPreferences.GetEntryValue<bool>(BuildInfo.Name, "SetupMode");
      if (!mSetupMode && setupMode) {
        if (mSensorManager != null) {
          foreach (var sensor in mSensorManager.Sensors) {
            if (sensor.OwnerType == SensorOwnerType.LocalPlayer || Util.Debug) {
              AddFrustumIfMissing(sensor);
            }
          }
        }
      } else if (mSetupMode && !setupMode) {
        foreach (var frustumObject in GameObject.FindObjectsOfTypeAll(Il2CppType.Of<OrthographicFrustum>())) {
          var frustum = frustumObject.Cast<OrthographicFrustum>();
          Util.DebugLog($"Destroyed debug frustum of \"{frustum.transform.parent.name}\" sensor");
          GameObject.Destroy(frustum.gameObject);
        }
      }
      mSetupMode = setupMode;

      if (MelonPreferences.GetEntryValue<bool>(BuildInfo.Name, "RestartIntiface")) {
        MelonPreferences.SetEntryValue(BuildInfo.Name, "RestartIntiface", false);
        RestartIntifaceServer();
      }
    }

    void StartIntifaceServer()
    {
      if (mIntifaceServer != null && !mIntifaceServer.HasExited) {
        return;
      }

      var intifaceConfigDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "IntifaceDesktop");
      var intifaceDir = File.ReadAllText(Path.Combine(intifaceConfigDir, "enginepath.txt"));
      var intifaceCLI = Path.Combine(intifaceDir, "IntifaceCLI.exe");

      mIntifaceServer = new Process();
      mIntifaceServer.StartInfo.FileName = intifaceCLI;
      var port = new Uri(mServerURI).Port;
      mIntifaceServer.StartInfo.Arguments = $"--servername \"{BuildInfo.Name} Server\" --wsinsecureport {port} --log warn";
      mIntifaceServer.StartInfo.UseShellExecute = false;
      mIntifaceServer.StartInfo.RedirectStandardOutput = true;
      mIntifaceServer.OutputDataReceived += (object sender, DataReceivedEventArgs e) => {
        if (e.Data != null) {
          Msg("[Intiface] " + e.Data);
        }
      };
      mIntifaceServer.StartInfo.RedirectStandardError = true;
      mIntifaceServer.ErrorDataReceived += (object sender, DataReceivedEventArgs e) => {
        if (e.Data != null) {
          Error("[Intiface] " + e.Data);
        }
      };
      mIntifaceServer.EnableRaisingEvents = true;
      mIntifaceServer.Exited += (object sender, EventArgs args) => {
        Warning("Intiface server process died.");
      };

      mIntifaceServer.Start();
      mIntifaceServer.BeginOutputReadLine();
      mIntifaceServer.BeginErrorReadLine();
    }

    async void RestartIntifaceServer()
    {
      if (mButtplug.Connected) {
        await mButtplug.DisconnectAsync();
      }

      if (mIntifaceServer != null && !mIntifaceServer.HasExited) {
        mIntifaceServer.Kill();
        mIntifaceServer = null;
      }
    }

    void ResetSensorParameterValues()
    {
      if (mSensorManager == null) {
        return;
      }

      foreach (var sensor in mSensorManager.Sensors) {
        if (sensor.Param != null) {
          sensor.Param.Value = 0;
        }
      }

      if (mGlobalParam != null) {
        mGlobalParam.Value = 0;
      }
    }

    async void DootDoot(ButtplugClientDevice device)
    {
      try {
        await device.SendVibrateCmd(0.15f);
        await Task.Delay(150);
        await device.SendStopDeviceCmd();
        await Task.Delay(100);
        await device.SendVibrateCmd(0.15f);
        await Task.Delay(150);
        await device.SendStopDeviceCmd();
      } catch { }
    }
  }
}
